// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
namespace SofaFabricNetCourse
{
	public abstract class upholtseryBase : intUpholstery
	{
		/// <summary>
		/// Gets the manufacturer.
		/// </summary>
		/// <value>The manufacturer.</value>
		public string manufacturer { get; private set;}
		/// <summary>
		/// Gets the name.
		/// </summary>
		/// <value>The name.</value>
		public string name { get; private set;}
		/// <summary>
		/// Gets the cost.
		/// </summary>
		/// <value>The cost.</value>
		public int cost { get; private set;}
		/// <summary>
		/// Initializes a new instance of the <see cref="NET1lab.upholtseryBase"/> class.
		/// </summary>
		/// <param name="Cost">Cost.</param>
		/// <param name="Name">Name.</param>
		/// <param name="Manufacturer">Manufacturer.</param>
		protected upholtseryBase (int Cost, string Name, string Manufacturer){
			cost = Cost;
			name = Name;
			manufacturer = Manufacturer;
		}
	}
}

