// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.IO;
using System.Threading;
namespace SofaFabricNetCourse
{
	public enum Type{
		Console, File
	}
	public class Logger<T> where T : intSofa<intUpholstery>
	{
		/// <summary>
		/// The sofa.
		/// </summary>
		private readonly T sofa;
		/// <summary>
		/// The file path.
		/// </summary>
		private readonly string filePath;
		/// <summary>
		/// Gets the type.
		/// </summary>
		/// <value>The type.</value>
		public Type Type { get; private set;}
		/// <summary>
		/// Occurs when on log.
		/// </summary>
		public event Action<TextWriter, T, SofaEventArgs> onLog;
		/// <summary>
		/// Initializes a new instance of the <see cref="NET1lab.Logger`1"/> class.
		/// </summary>
		/// <param name="sofa">Sofa.</param>
		/// <param name="filePath">File path.</param>
		public Logger (T sofa, string filePath = null)
		{
			this.sofa = sofa;
			if (filePath == null) {
				Type = Type.Console;
			} else {
				Type = Type.File;
				this.filePath = filePath;
				if (!File.Exists(filePath)){
					File.Create(filePath);
				}
			}

			sofa.onOpen += Sofa_EventHandler;
			sofa.onClose += Sofa_EventHandler;
		}
        /// <summary>
        /// Sofa_s the event handler.
        /// </summary>
        /// <param name="args">Arguments.</param>
        /// 
        private readonly object locker = new object();
        private void Sofa_EventHandler(SofaEventArgs args)
        {
            var ts = new ThreadStart(() =>
            {
                lock (locker)
                {
                    using (var writer = GetWriter())
                    {
                        onLog.Invoke(writer, sofa, args);
                    }
                }
            });
			var fake = new ThreadStart(() =>
		  {
			 
		  });
			var thread1 = new Thread(fake) { IsBackground = true };
			var thread2 = new Thread(fake) { IsBackground = true };
			var thread3 = new Thread(fake) { IsBackground = true };
			var thread4 = new Thread(fake) { IsBackground = true };
			var thread5 = new Thread(fake) { IsBackground = true };
			var thread6 = new Thread(ts) { IsBackground = true };
			//System.Console.WriteLine(thread.ManagedThreadId);
            thread1.Start();
			thread2.Start();
			thread3.Start();
			thread4.Start();
			thread5.Start();
			thread6.Start();

		}
		/// <summary>
		/// Gets the writer.
		/// </summary>
		/// <returns>The writer.</returns>
		private TextWriter GetWriter(){
			if (Type == Type.Console) {
				return Console.Out;
			}else{
				 return File.AppendText (filePath);
			}
		}
		/// <summary>
		/// Creates the console logger for sofa.
		/// </summary>
		/// <returns>The console logger for sofa.</returns>
		/// <param name="sofa">Sofa.</param>
		public static Logger<T> createConsoleLoggerForSofa(T sofa){
			return new Logger<T> (sofa);
		}
		/// <summary>
		/// Creates the file loggerfor sofa.
		/// </summary>
		/// <returns>The file loggerfor sofa.</returns>
		/// <param name="sofa">Sofa.</param>
		/// <param name="path">Path.</param>
		public static Logger<T> createFileLoggerforSofa(T sofa, string path){
			return new Logger<T> (sofa, path);
		}
	}
}

