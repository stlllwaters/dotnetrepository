// ------------------------------------------------------------------------------
//  <autogenerated>
//      This code was generated by a tool.
//      Mono Runtime Version: 4.0.30319.1
// 
//      Changes to this file may cause incorrect behavior and will be lost if 
//      the code is regenerated.
//  </autogenerated>
// ------------------------------------------------------------------------------
using System;
using System.Collections.Generic;
namespace SofaFabricNetCourse
{
	public interface intSofa<out T> :IEquatable<intSofa<intUpholstery>> where T :intUpholstery
	{ 
		/// <summary>
		/// Occurs when on open.
		/// </summary>
		event Action<SofaEventArgs> onOpen;
		/// <summary>
		/// Occurs when on close.
		/// </summary>
		event Action<SofaEventArgs> onClose;
		/// <summary>
		/// Gets the model.
		/// </summary>
		/// <value>The model.</value>
		string model { get; }
		/// <summary>
		/// Gets the width expanded.
		/// </summary>
		/// <value>The width expanded.</value>
		float widthExpanded { get ;}
		/// <summary>
		/// Gets the width.
		/// </summary>
		/// <value>The width.</value>
		float width { get ;}
		/// <summary>
		/// Gets the width closed.
		/// </summary>
		/// <value>The width closed.</value>
		float widthClosed { get;}
		/// <summary>
		/// Gets the height.
		/// </summary>
		/// <value>The height.</value>
		float height { get ;}	
		/// <summary>
		/// Prepares for sleep.
		/// </summary>
		void prepareForSleep() ;
		/// <summary>
		/// Close this instance.
		/// </summary>
		void close() ;
		/// <summary>
		/// Gets the upholstery.
		/// </summary>
		/// <value>The upholstery.</value>
		T upholstery{ get;}
	}
}

